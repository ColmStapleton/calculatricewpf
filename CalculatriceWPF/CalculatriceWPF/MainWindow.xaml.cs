﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CalculatriceWPF
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void AC(object sender, RoutedEventArgs e)
        {
            Affichage.Text = "";
        }

        private void PlusMoins(object sender, RoutedEventArgs e)
        {
            //A Faire
        }

        private void Prcent(object sender, RoutedEventArgs e)
        {
            try
            {
                double affichage = double.Parse(Affichage.Text);
                affichage *= 100;
                String affichageString = affichage.ToString();
                affichageString += "%";
                Affichage.Text = affichage.ToString();
            }
            catch
            {
                MessageBox.Show("Opération impossible");
                Affichage.Text = "";
            }

        }

        private void Affiche(object sender, RoutedEventArgs e)
        {
            Button but = (Button)sender;
            Affichage.Text += but.Content; 
        }

        private void Egal(object sender, RoutedEventArgs e)
        {
            //String[] ops = Regex.Split(Affichage.Text, @"\d+"); 
            String[] ops = Regex.Split(Affichage.Text, @"[0-9,]+");
            List<String> opsList = ops.ToList();

            opsList.RemoveAll(vide);

            String[] valeursString = Regex.Split(Affichage.Text, @"[\+\-\*\/]");
            List<double> valeursNum = new List<double>();
            double res = 0;

            foreach (String elt in valeursString)
            {
                double num = double.Parse(elt);
                valeursNum.Add(num);
            }

            int i = 0;
            while(i < opsList.Count)
            {
                if (opsList[i].Contains("*"))
                {
                    res = valeursNum[i] * valeursNum[i+1];
                    valeursNum[i] = res;
                    valeursNum.RemoveAt(i+1);
                    opsList.RemoveAt(i);
                    i = 0;
                }
                else if (opsList[i].Contains("/"))
                {
                    res = valeursNum[i] / valeursNum[i+1];
                    valeursNum[i] = res;
                    valeursNum.RemoveAt(i+1);
                    opsList.RemoveAt(i);
                    i = 0;
                }
                else
                {
                    i++;
                }
            }

            i = 0;
            while (i < opsList.Count)
            {
                if (opsList[i].Contains("+"))
                {
                    res = valeursNum[i] + valeursNum[i+1];
                    valeursNum[i] = res;
                    valeursNum.RemoveAt(i+1);
                    opsList.RemoveAt(i);
                    i = 0;
                }
                else if (opsList[i].Contains("-"))
                {
                    res = valeursNum[i] - valeursNum[i+1];
                    valeursNum[i] = res;
                    valeursNum.RemoveAt(i+1);
                    opsList.RemoveAt(i);
                    i = 0;
                }
                else
                {
                    i++;
                }
            }

            Affichage.Text = res.ToString();
        }

        private static bool vide(String s)
        {
            return s.Equals("");
        }
    }
}
